#! /bin/sh
if [ "${S3_S3V4}" = "true" ]
then
  aws configure set default.s3.signature_version s3v4
fi

if [ "${S3_ACCESS_KEY_ID}" = "" ]
then
  echo "You need to set the S3_ACCESS_KEY_ID environment variable."
  exit 1
fi

if [ "${S3_SECRET_ACCESS_KEY}" = "" ]
then
  echo "You need to set the S3_SECRET_ACCESS_KEY environment variable."
  exit 1
fi

if [ "${S3_BUCKET}" = "" ]
then
  echo "You need to set the S3_BUCKET environment variable."
  exit 1
fi

if [ "${MYSQL_DATABASE}" = "" ]
then
  echo "You need to set the MYSQL_DATABASE environment variable."
  exit 1
fi

if [ "${MYSQL_HOSTNAME}" = "" ]
then
  if [ -n "${MYSQL_PORT_3306_TCP_ADDR}" ]
  then
    MYSQL_HOSTNAME=$MYSQL_PORT_3306_TCP_ADDR
    MYSQL_PORT=$MYSQL_PORT_3306_TCP_PORT
  else
    echo "You need to set the MYSQL_HOSTNAME environment variable."
    exit 1
  fi
fi

if [ "${MYSQL_USER}" = "" ]
then
  echo "You need to set the MYSQL_USER environment variable."
  exit 1
fi

if [ "${MYSQL_PASSWORD}" = "" ]
then
  echo "You need to set the MYSQL_PASSWORD environment variable."
  exit 1
fi

if [ "${S3_ENDPOINT}" = "" ]
then
  AWS_ARGS=""
else
  AWS_ARGS="--endpoint-url ${S3_ENDPOINT}"
fi

export AWS_ACCESS_KEY_ID=$S3_ACCESS_KEY_ID
export AWS_SECRET_ACCESS_KEY=$S3_SECRET_ACCESS_KEY
export AWS_DEFAULT_REGION=$S3_REGION

MYSQL_HOSTNAME_OPTS="-h $MYSQL_HOSTNAME -P $MYSQL_PORT -u $MYSQL_USER -p$MYSQL_PASSWORD $MYSQL_EXTRA_OPTS"

echo "Creating dump of ${MYSQL_DATABASE} database from ${MYSQL_HOSTNAME}..."

mysqldump $MYSQL_HOSTNAME_OPTS $MYSQL_DATABASE | gzip > dump.sql.gz

echo "Uploading dump to bucket $S3_BUCKET"

cat dump.sql.gz | aws $AWS_ARGS s3 cp - s3://$S3_BUCKET/$S3_PREFIX/${MYSQL_DATABASE}_$(date +"%Y-%m-%dT%H:%M:%SZ").sql.gz || exit 2

echo "Database backup uploaded successfully"
exit 0