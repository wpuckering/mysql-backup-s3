FROM alpine:3.11
LABEL maintainer="William Puckering <william.puckering@gmail.com>"

RUN apk add --no-cache \
    python \
    py2-pip \
    mysql-client=10.4.10-r0 \
  && pip install awscli \
  && apk del py2-pip

ENV MYSQL_DATABASE ''
ENV MYSQL_HOSTNAME ''
ENV MYSQL_PORT 5432
ENV MYSQL_USER ''
ENV MYSQL_PASSWORD ''
ENV MYSQL_EXTRA_OPTS ''
ENV S3_ACCESS_KEY_ID ''
ENV S3_SECRET_ACCESS_KEY ''
ENV S3_BUCKET ''
ENV S3_REGION 'us-west-1'
ENV S3_PREFIX ''
ENV S3_ENDPOINT ''
ENV S3_S3V4 'true'

ADD run.sh run.sh

CMD ["sh", "run.sh"]